# pystrf
Python based radio frequency satellite tracking.

This is a python port of STRF (https://github.com/cbassa/strf), but will include additional functionality to work with SatNOGS HDF5 artifacts and advanced Doppler curve detection algorithms.

## Installation

Install dependencies with
```
pip install -r requirements.txt
```
Install the git large filesystem to download example spectra and run
```
git lfs init
```
Clone the repository via HTML
```
git clone https://gitlab.com/librespacefoundation/pystrf.git
```
or via SSH (you may have to add SSH keys to your gitlab account)
```
git clone git@gitlab.com:librespacefoundation/pystrf.git
```

## Usage

To view data attached as example:

```
./rfplot.py -p data/2021-08-04T20_48_35 -c tle/classfd.tle
```
